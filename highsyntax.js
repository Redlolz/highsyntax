// var strReg1 = /"(.*?)"/g,
//     strReg2 = /'(.*?)'/g,
//     specialReg = /\b(new|var|if|do|function|while|switch|for|foreach|in|continue|break)(?=[^\w])/g,
//     specialJsGlobReg = /\b(document|window|Array|String|Object|Number|\$)(?=[^\w])/g,
//     specialJsReg = /\b(getElementsBy(TagName|ClassName|Name)|getElementById|typeof|instanceof)(?=[^\w])/g,
//     specialMethReg = /\b(indexOf|match|replace|toString|length)(?=[^\w])/g,
//     specialPhpReg = /\b(define|echo|print_r|var_dump)(?=[^\w])/g,
//     specialCommentReg = /(\/\*.*\*\/)/g,
//     inlineCommentReg = /(\/\/.*)/g;
//
// codeElements.each(function() {
//     var string = this.innerHTML;
//
//     parsed = string.replace(strReg1, '<span class="string">"$1"</span>');
//     parsed = parsed.replace(strReg2, "<span class=\"string\">'$1'</span>");
//     parsed = parsed.replace(specialReg, '<span class="special">$1</span>');
//     parsed = parsed.replace(specialJsGlobReg, '<span class="special-js-glob">$1</span>');
//     parsed = parsed.replace(specialJsReg, '<span class="special-js">$1</span>');
//     parsed = parsed.replace(specialMethReg, '<span class="special-js-meth">$1</span>');
//     parsed = parsed.replace(htmlTagReg, '<span class="special-html">$1</span>');
//     parsed = parsed.replace(sqlReg, '<span class="special-sql">$1</span>');
//     parsed = parsed.replace(specialPhpReg, '<span class="special-php">$1</span>');
//     parsed = parsed.replace(specialCommentReg, '<span class="special-comment">$1</span>');
//     parsed = parsed.replace(inlineCommentReg, '<span class="special-comment">$1</span>');
//
//     this.innerHTML = parsed;
// });

var strReg1 = /"(.*?)"/g,
    strReg2 = /'(.*?)'/g;

/* Python */


function python() {
    htmltag = document.getElementById('code');
    code = htmltag.innerHTML;

    for (var i = 0; i < code.length; i++) {
        code[i] = code[i].replace(/def/g, '<span class="python-def">def</span>');
        code[i] = code[i].replace(/import/g, '<span class="python-import">import</span>');
        code[i] = code[i].replace(/from/g, '<span class="python-from">from</span>');
    }
    htmltag.innerHTML = code.join('\n');
}
